/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gotests;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author Andres
 */
public class Gotests {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        System.out.println("Esto es un hola mundo para probar git");
        System.out.println("Ingresa tu nombre\n");
        BufferedReader in=new BufferedReader(new InputStreamReader(System.in));
        String nombre=in.readLine();
        System.out.println("Hola "+nombre);
        System.out.println("Fin");
    }
    
}
